-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 01, 2021 at 07:38 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(16, 'Molestiae natus sequi sunt et.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(17, 'Sit aut sunt sunt voluptas.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(20, 'Est suscipit aperiam dolores vel cum itaque.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(21, 'Et ut deleniti est qui ut incidunt non.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(22, 'Itaque iste quasi iusto quod. A non ducimus sunt.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(23, 'Repudiandae est repudiandae et quia id.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(24, 'In maxime dicta commodi.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(25, 'Cumque corrupti dicta quidem molestias ut.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(26, 'Cumque est qui voluptatem voluptas in non quia.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(27, 'Consequatur sed soluta modi ut dicta in libero.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(28, 'Et sit voluptatibus blanditiis qui maiores iure.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(29, 'Maxime esse dolores qui quia nobis deleniti.', '2021-03-25 03:42:37', '2021-03-25 03:42:37'),
(30, 'Illo iusto omnis nihil.', '2021-03-25 03:42:38', '2021-03-25 03:42:38'),
(31, 'Md. Shahinur Islam', '2021-03-28 21:19:43', '2021-03-28 21:19:43'),
(32, 'Message', '2021-03-28 21:22:20', '2021-03-28 21:22:20'),
(33, 'Md. Shahinur Islam', '2021-03-28 21:31:39', '2021-03-28 21:31:39'),
(34, 'Md. Shahinur Islam', '2021-03-28 21:40:37', '2021-03-28 21:40:37'),
(35, 'shahin', '2021-03-28 21:47:44', '2021-03-29 04:25:59'),
(36, 'Md. Shahinur Islam', '2021-03-29 04:26:54', '2021-03-29 04:27:34'),
(37, 'Md. Shahinur Islam', '2021-03-29 05:27:06', '2021-03-29 05:27:06');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_03_22_094625_create_categories_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `lastname`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '1234asdf', 'Shahin', 'shahin@gmail.com', '2021-03-02 03:03:54', '1234', NULL, '2021-03-01 03:03:54', '2021-03-31 22:48:26'),
(2, 'Md. Shahinur Islamf', '', 'demo@gmail.com', NULL, '123456', NULL, '2021-03-30 21:49:33', '2021-03-31 21:38:12'),
(4, 'Md. Shahinur Islam', '', 'qqdemo@gmail.com', NULL, '112233', NULL, '2021-03-31 21:08:15', '2021-03-31 21:08:15'),
(5, 'Md. Shahinur Islam', '', 'deffmo@gmail.com', NULL, 'ffffff', NULL, '2021-03-31 21:17:55', '2021-03-31 21:17:55'),
(6, 'Md. Shahinur Islamddfgfg', '', 'dedddmo@gmail.com', NULL, 'dddddd', NULL, '2021-03-31 21:22:49', '2021-03-31 21:22:49'),
(7, 'Message', '', 'demosfsdf@gmail.com', NULL, '11111111', NULL, '2021-03-31 21:45:08', '2021-03-31 21:45:08'),
(8, 'Md. Shahinur Islam', '', 'dedfsdfmo@gmail.com', NULL, 'ffffff', NULL, '2021-03-31 21:47:33', '2021-03-31 21:47:33'),
(9, 'Md. Shahinur Islam', '', 'demrerero@gmail.com', NULL, '111111', NULL, '2021-03-31 21:51:51', '2021-03-31 21:51:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
