<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/',function(){
    return view("index");
});

Route::get('/dashboard',function(){
    return view("admin/index");
});

// for login
Route::get('/login',function(){
    return view("admin/auth/login");
});

Route::post('/login/check', [HomeController::class, 'login']);

//Route::get('/', [HomeController::class, 'welcome']);

Route::get('/about',  [HomeController::class, 'about']);

Route::get('/contact',[HomeController::class, 'contact']);

//----------------- for admin pages ----------------------
// category

Route::get('/dashboard/categories/create', [CategoriesController::class, 'create']);

Route::get('/dashboard/categories/viewall', [CategoriesController::class, 'index']);

Route::post('/dashboard/categories', [CategoriesController::class, 'store']);

Route::get('/dashboard/categories/{id}', [CategoriesController::class, 'show']);

Route::get('/dashboard/categories/{id}/edit', [CategoriesController::class, 'edit']);

Route::patch('/dashboard/categories/{id}', [CategoriesController::class, 'update']);

Route::delete('/dashboard/categories/{id}', [CategoriesController::class, 'destroy']);

// for user

Route::get('/user/create', [UsersController::class, 'create']);

Route::post('/user/create/store', [UsersController::class, 'store']);

Route::get('/user/create/viewall', [UsersController::class, 'index']);

Route::get('/dashboard/user/{id}', [UsersController::class, 'show']);

Route::get('/dashboard/user/{id}/edit', [UsersController::class, 'edit']);

Route::patch('/dashboard/user/edit/{id}', [UsersController::class, 'update']);

Route::delete('/dashboard/user/{id}', [UsersController::class, 'destroy']);
//Route::get('/user/{id}', [HomeController::class, 'user']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
