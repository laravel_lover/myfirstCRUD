@extends('admin.layouts.master')

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>
    <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
        For more information about DataTables, please visit the <a target="_blank"
            href="https://datatables.net">official DataTables documentation</a>.</p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">DataTables<span>
               @if(session()->has('message'))
                    {{ session('message')}}
               @endif 
            </span></h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Age</th>
                            <th>Start date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Sl.</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Office</th>
                            <th>Age</th>
                            <th>Start date</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @php
                        $sl = 0;
                        @endphp
                        @foreach($categories as $category)
                        <tr>
                            <td>{{ ++$sl }}</td>
                            <td>{{ $category->name }}</td>
                            <td>Customer Support</td>
                            <td>New York</td>
                            <td>27</td>
                            <td>2011/01/25</td>
                            <td><a href="{{ url('dashboard/categories/'.$category->id ) }}"><i class="fas fa-eye"></i>

                            </a>|<a href="{{ url('dashboard/categories/'.$category->id.'/edit' ) }}"> <i class="fas fa-edit"></i>

                            </a>
                            <a>
                                {!! Form::open(['url'=>'dashboard/categories/'.$category->id, 'method'=>'delete']) !!}

                                 
                                 <button type="submit" class="btn btn-link"><i class="fas fa-trash"></i></button>


                                 {!! Form::close() !!}
                            </a>
                            </td>
                        </tr>
                     @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection