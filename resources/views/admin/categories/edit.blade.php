@extends('admin.layouts.master')

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Page</h1>
        <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50"></i> Generate Report</a>
    </div>

    <!-- Content Row -->
    <div class="container">

        <div class="card o-hidden border-0 shadow-lg my-5">
            <div class="card-body p-0">
                <!-- Nested Row within Card Body -->
                <div class="row justify-content-md-center">
                     
                    <div class="col-lg-7 ">
                        <div class="p-5">
                            <div class="text-center">
                                <h1 class="h4 text-gray-900 mb-4">Edit an Categories!</h1>
                            </div>
                            {!! Form::open(['url' => '/dashboard/categories/'.$categories->id,'method'=>'patch']) !!}
                            
                        
                            {{-- <form action="{{ url('/dashboard/categories') }}" method="POST" class="user"> --}}
                                {{-- {{ csrf_field() }} --}}
                                <div class="form-group row">
                                    <div class="col-sm-12 mb-6 mb-sm-12">
                                        <input type="text" value="{{ $categories->name }}" name="name" class="form-control form-control-user" id="exampleFirstName"
                                            placeholder="First Name">
                                    </div>                                   
                                </div>
                                <button type="submit" class="btn btn-primary btn-user btn-block">
                                    Create
                                </button>
                                <div class="form-group row">
                                    @foreach ($errors->all() as $messages)
                                        {{ $messages }}
                                    @endforeach
                                </div>
                            {{-- </form> --}}

                            {!! Form::close() !!}
                            <hr>
                            <div class="text-center">
                                <a class="small" href="forgot-password.html">Forgot Password?</a>
                            </div>
                            <div class="text-center">
                                <a class="small" href="login.html">Already have an account? Login!</a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Content Row -->


</div>
@endsection