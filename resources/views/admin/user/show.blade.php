@extends('admin.layouts.master')

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>
    <p class="mb-4"></p>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Category Show</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Sl.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>Create</th>
                            <th>Update</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Sl.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Password</th>
                            <th>Create</th>
                            <th>Update</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        
                        <tr>
                            <td>1</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->password }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>{{ $user->updated_at }}</td>
                            <td><a href="{{ url('dashboard/user/'.$user->id.'/edit' ) }}"> <i class="fas fa-edit"></i>

                            </a>
                            <a>
                                {!! Form::open(['url'=>'dashboard/user/'.$user->id, 'method'=>'delete']) !!}

                                 
                                 <button type="submit" class="btn btn-link"><i class="fas fa-trash"></i></button>


                                 {!! Form::close() !!}
                            </a></td>
                        </tr>                     

                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection