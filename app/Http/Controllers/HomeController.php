<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class HomeController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }
    public function about() {
    
        return view('about');
    }
    public function contact() {    
        return view('contact');
    }
    public function user($id) {    
        return view('users.show',compact('id'));
    }



    public function __construct() {
        $this->middleware('auth');
     }
     
     /**
        * Show the application dashboard.
        *
        * @return \Illuminate\Http\Response
     */
     
    public function login (Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                $response = ['token' => $token];
                return response($response, 200);
            } else {
                $response = ["message" => "Password mismatch"];
                return response($response, 422);
            }
        } else {
            $response = ["message" =>'User does not exist'];
            return response($response, 422);
        }
    }
}
